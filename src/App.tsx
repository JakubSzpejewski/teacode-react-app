import React, { Component } from 'react';
import './App.css';
import { List, ListItem, Checkbox, Typography, ListItemSecondaryAction, TextField, Divider } from '@material-ui/core';
import Contact, { ContactModel } from './contact/contact';

class App extends Component<{}, { contacts: ContactModel[], selected: ContactModel[], loaded: boolean, error?: string, searchMatch: RegExp }> {
  constructor(props: {}) {
    super(props);
    this.state = {
      contacts: [],
      selected: [],
      loaded: false,
      error: undefined,
      searchMatch: new RegExp(''),
    };
  }

  public componentDidMount(): void {
    fetch('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json')
      .then(res => res.json())
      .then(
        (json: ContactModel[]) => {
          const contacts = json.sort((a, b) => (a.last_name > b.last_name) ? 1 : -1);
          this.setState({ loaded: true, contacts });
        },
        (error) => {
          this.setState({ loaded: true, error: error.message });
        });
  }

  private onToggle = (toggledContact: ContactModel) => () => {
    const index = this.state.selected.indexOf(toggledContact);
    const selectedCopy = [...this.state.selected];

    if (index === -1) {
      selectedCopy.push(toggledContact);
    } else {
      selectedCopy.splice(index, 1);
    }

    const selectedIds = selectedCopy.map((v) => v.id);
    console.log(selectedIds);

    this.setState({ selected: selectedCopy });
  }

  private onSearchChange(searchText: string): void {
    const searchMatch = new RegExp(searchText, 'i');
    this.setState({ searchMatch });
  }

  public render() {
    if (this.state.error) {
      return (
        <div className="App">
          <Typography>Error: {this.state.error}</Typography>
        </div>
      );
    } else if (!this.state.loaded) {
      return (
        <div className="App">
          <Typography>Loading...</Typography>
        </div>
      );
    } else {
      const showingContacts = this.state.contacts.filter(contact => this.state.searchMatch.test(contact.first_name) || this.state.searchMatch.test(contact.last_name));
      return (
        <div className="App">
          <TextField type='search' label='Search' onChange={(event) => this.onSearchChange(event.target.value)} />
          <List dense>
            {showingContacts.map((contact) => {
              return (
                <div>
                  <ListItem button key={contact.id} onClick={this.onToggle(contact)}>
                    <Contact contact={contact} ></Contact>
                    <ListItemSecondaryAction>
                      <Checkbox
                        onChange={this.onToggle(contact)}
                        checked={this.state.selected.indexOf(contact) !== -1}
                      />
                    </ListItemSecondaryAction>
                  </ListItem>
                  <Divider />
                </div>
              )
            })}
          </List>
        </div>);
    }
  }
}

export default App;
