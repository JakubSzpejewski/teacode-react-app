import React from 'react';
import './contact.css';
import { Avatar, Box, Typography } from '@material-ui/core';

export interface ContactModel {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    gender: string;
    avatar: string;
}

class Contact extends React.Component<{ contact: ContactModel }> {
    public render() {
        return (
            <Box className='Contact-wrapper'>
                <Avatar className='Contact-Avatar' src={this.props.contact.avatar}></Avatar>
                <Box className='Contact-details'>
                    <Typography className='Contact-name'>{this.props.contact.first_name} {this.props.contact.last_name}</Typography>
                    <Typography className='Contact-mail'>{this.props.contact.email}</Typography>
                </Box>
            </Box>
        );
    }
}

export default Contact;